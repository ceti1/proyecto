<?php

use Illuminate\Database\Seeder;
use App\Person;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Person::class, 30)->create();

        // $persona = new Person();
        // $persona->nombre = "Rubén";
        // $persona->apellido_paterno = "Chanamé";
        // $persona->apellido_materno = "Sánchez";
        // $persona->email = "rdchs.256@gmail";
        // $persona->save();  // encargado de registrar: INSERT INTO () VALUES ()

        // $persona2 = new Person();
        // $persona2->nombre = "Rubén";
        // $persona2->apellido_paterno = "Chanamé";
        // $persona2->apellido_materno = "Sánchez";
        // $persona2->email = "rdchs.257@gmail";
        // $persona2->save();
    }
}
