<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPeople extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id(); // un campo ID, es BIG INTEGER, es PK, UNSIGNED, AUTOINCREMENT
            $table->string('nombre', 50);
            $table->string('apellido_paterno', 50);
            $table->string('apellido_materno', 50);
            $table->string('email', 50)->unique();
            $table->string('celular', 30)->nullable();
            $table->timestamps();
            // created_at: timestamp: 2020-08-25 19:54:00 => hora en que se cree el registro
            // updated_at: timestamp: 2020-08-25 19:54:00 => hora de la ultima actualizacion
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
