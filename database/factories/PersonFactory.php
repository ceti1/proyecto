<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Person;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {
    return [
        'nombre' => $faker->firstName(),
        'apellido_paterno' => $faker->lastName(),
        'apellido_materno' => $faker->lastName(),
        'email' => $faker->unique()->email(),
    ];
});
