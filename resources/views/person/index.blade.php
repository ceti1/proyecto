@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Personas</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellido paterno</th>
                                    <th>Apellido materno</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($personas as $elemento)
                                    <tr>
                                        <td>{{ $elemento->nombre }}</td>
                                        <td>{{ $elemento->apellido_paterno }}</td>
                                        <td>{{ $elemento->apellido_materno }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                        {{ $personas->links() }}
                        {{ $personas->count() }}
                        {{ $personas->currentPage() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
