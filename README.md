## Configuración

### Paso 01:  Instalar paquetes

```
composer install
```
### Paso 02: Crear archivo de variables de entorno
```
Copiar archivo .env.example a un archivo .env 
```
### Paso 03: Generar KEY de la aplicación
```
php artisan key:generate
```
### Paso 04: Instalar paquetes para la vista (tener instalado NODE.js)
```
npm install
```
### Paso 05: Generar archivos estáticos
```
npm run dev
```