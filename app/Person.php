<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Person => Nombre del modelo
class Person extends Model
{
    // TipoPersona => TipoPersonas => tipo_personas
    // camelCase
    // PascalCase
    // protected $table = "tipopersonas";
    // SnakeCase => snake_case
    // TipoPersona => TipoPersonas => tipo_personas
    // protected $table = "nombre_de_la_tabla";
    // Person => people
    // Avion => avions
    // Man => men
    // Computadora => computadoras

    // Laravel por defecto asume que mi tabla es PEOPLE
    // protected $table = 'people'; // person => people, Car => cars, 
    // avion => avions, colegio => colegios, man => men

    // Laravel sobre entiende que tu PK, se llama ID
    // protected $primaryKey = 'persona_id'; // id

    // 
    // protected $incrementing = false;

    // asume que el ID es biginteger;
    // protected $keyType = 'string';

    // asume que existen los campos created_at y updated_at
    // protected $timestamps = false;

    // const UPDATED_AT = 'actualizado';
    // const CREATED_AT = 'creado';
}
